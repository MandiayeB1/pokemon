import org.junit.jupiter.api.Assertions;

public class MainTest {
    public void testBidon() {
        // GIVEN
        int a = 2;
        int b = 3;

        // WHEN
        int c = a + b;

        // THEN
        Assertions.assertEquals(5, c);
    }
}
