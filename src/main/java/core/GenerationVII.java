package core;

public class GenerationVII {
    Icons IconsObject;
    UltraSunUltraMoon UltraSunUltraMoonObject;

    public Icons getIconsObject() {
        return IconsObject;
    }

    public void setIconsObject(Icons iconsObject) {
        IconsObject = iconsObject;
    }

    public UltraSunUltraMoon getUltraSunUltraMoonObject() {
        return UltraSunUltraMoonObject;
    }

    public void setUltraSunUltraMoonObject(UltraSunUltraMoon ultraSunUltraMoonObject) {
        UltraSunUltraMoonObject = ultraSunUltraMoonObject;
    }
}
