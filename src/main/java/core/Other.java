package core;

public class Other {
    DreamWorld DreamWorldObject;
    OfficialArtwork OfficialArtworkObject;

    public DreamWorld getDreamWorldObject() {
        return DreamWorldObject;
    }

    public void setDreamWorldObject(DreamWorld dreamWorldObject) {
        DreamWorldObject = dreamWorldObject;
    }

    public OfficialArtwork getOfficialArtworkObject() {
        return OfficialArtworkObject;
    }

    public void setOfficialArtworkObject(OfficialArtwork officialArtworkObject) {
        OfficialArtworkObject = officialArtworkObject;
    }
}
