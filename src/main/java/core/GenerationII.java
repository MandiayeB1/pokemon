package core;

public class GenerationII {
    Crystal CrystalObject;
    Gold GoldObject;
    Silver SilverObject;


    // Getter Methods

    public Crystal getCrystal() {
        return CrystalObject;
    }

    public Gold getGold() {
        return GoldObject;
    }

    public Silver getSilver() {
        return SilverObject;
    }

    // Setter Methods

    public void setCrystal(Crystal crystalObject) {
        this.CrystalObject = crystalObject;
    }

    public void setGold(Gold goldObject) {
        this.GoldObject = goldObject;
    }

    public void setSilver(Silver silverObject) {
        this.SilverObject = silverObject;
    }
}
