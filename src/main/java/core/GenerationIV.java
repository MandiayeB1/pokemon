package core;

public class GenerationIV {
    DiamondPearl DiamondPearlObject;
    HeartgoldSoulsilver HeartgoldSoulsilverObject;
    Platinum PlatinumObject;

    public DiamondPearl getDiamondPearlObject() {
        return DiamondPearlObject;
    }

    public void setDiamondPearlObject(DiamondPearl diamondPearlObject) {
        DiamondPearlObject = diamondPearlObject;
    }

    public HeartgoldSoulsilver getHeartgoldSoulsilverObject() {
        return HeartgoldSoulsilverObject;
    }

    public void setHeartgoldSoulsilverObject(HeartgoldSoulsilver heartgoldSoulsilverObject) {
        HeartgoldSoulsilverObject = heartgoldSoulsilverObject;
    }

    public Platinum getPlatinumObject() {
        return PlatinumObject;
    }

    public void setPlatinumObject(Platinum platinumObject) {
        PlatinumObject = platinumObject;
    }
}
