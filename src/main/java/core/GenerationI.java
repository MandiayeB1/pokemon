package core;

public class GenerationI {
    RedBlue RedBlueObject;
    Yellow YellowObject;

    public RedBlue getRedBlueObject() {
        return RedBlueObject;
    }

    public void setRedBlueObject(RedBlue redBlueObject) {
        RedBlueObject = redBlueObject;
    }

    public Yellow getYellowObject() {
        return YellowObject;
    }

    public void setYellowObject(Yellow yellowObject) {
        YellowObject = yellowObject;
    }

}
